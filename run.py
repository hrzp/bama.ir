from flask import Flask, redirect, request
import json
from api import bama

app = Flask(__name__)

@app.route('/')
def home():
    return redirect('/static/index.html')


@app.route('/links', methods=['GET','POST'])
def links():
    try:
        pages_num = bama().available_pages()
        return str(pages_num), 200
    except Exception, e:
        raise e
        return 'error', 400


@app.route('/get_urls',  methods=['GET','POST'])
def get_urls():
    data = json.loads(request.data)
    bama().call(int(data['num']))
    return 'ok',200


@app.route('/get_files', methods=['GET','POST'])
def get_files():
    return json.dumps(bama().get_files())


@app.route('/do_extract',  methods=['GET','POST'])
def do_extract():
    data = json.loads(request.data)
    bama().do_extract(data['file'])
    return 'ok',200
if __name__ == '__main__':
    app.run(debug=False)

