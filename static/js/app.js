var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope,$http) {
    $scope.links = '0';
    $scope.num_select = 0;
    $scope.panel = 'wait';
    $scope.progress = 1;
    $http.post('/links').success(function(res) {
        $scope.links = res;
        $scope.panel = 'main';
    })
    .error(function(res) {
        alert('خطلا در ارتباط با سایت لطفا برنامه را دوباره اجرا کنید');
    });


    $scope.get_links = function(){
        $scope.panel = 'wait';
        if ($scope.num_select == 0){
            var data = {'num': $scope.links}
        }
        else{
            if ($scope.num_select > $scope.links){
                alert('عدد انتخابی شما بیشتر از لینک های موجود می باشد');
                $scope.panel = 'main';
                return;
            }
            var data = {'num': $scope.num_select} 
        }
        $scope.progress = (data.num * 0.1) / 60;
        $scope.progress = parseFloat($scope.progress).toPrecision(5);
        $http.post('/get_urls', data).success(function(res) {
            alert('لینک ها با موفقیت در پوشه لینک ها ذخیر شد');
            $scope.panel = 'main';
        })
        .error(function(res) {
            alert('خطلا در ارتباط با سایت لطفا برنامه را دوباره اجرا کنید');
            $scope.panel = 'main';
        });
    };


    $scope.show_files = function(){
        $http.post('/get_files').success(function(res) {
            $scope.panel = 'phone';
            $scope.files = res;
        })
        .error(function(res) {
            alert('خطلا در ارتباط با سایت لطفا برنامه را دوباره اجرا کنید');
            $scope.panel = 'main';
        });
    };


    $scope.do_extract = function(file){
        var data = {'file': file};
        $scope.panel = 'wait';
        $http.post('/do_extract', data).success(function(res) {
            $scope.panel = 'phone';
            alert('شماره ها در پوشه تلفن ذخیره شد');
        })
        .error(function(res) {
            alert('خطلا در ارتباط با سایت لطفا برنامه را دوباره اجرا کنید');
            $scope.panel = 'phone';
        });
    };
});