# encoding: utf8

from bs4 import BeautifulSoup
import mechanize
import time, datetime, os
import thread
import threading


class bama(object):
    """docstring for bama"""
    def __init__(self):
        super(bama, self).__init__()
        self.url = 'http://bama.ir/car'
        self.browser = mechanize.Browser()
        self.links_num = 0
        self.links = []
        self.crash_page_id = []
        self.set_browser()
        self.phone_list = []


    def set_browser(self):
        self.browser.set_handle_equiv(True)
        self.browser.set_handle_redirect(True)
        self.browser.set_handle_referer(True)
        self.browser.set_handle_robots(False)
        self.browser.set_handle_refresh(mechanize.HTTPRefreshProcessor(), max_time=1)


    def available_pages(self):
        self.browser.open(self.url)
        html = self.browser.response().read()
        doc = BeautifulSoup(html, 'html.parser')
        links = ''
        try:
            link = doc.select('.paging-bottom-div > h4')
            link = link[0].text.encode('utf-8').split(' ')[-2]
            for i in link:
                if i.isdigit():
                    links += i
            self.links = int(links)
        except:
            pass
        return links


    def call(self, links):
        t1=datetime.datetime.now()
        links = links / 12
        def do_thread(start,stop):
            threads = []
            for i in range(start, stop):
                print 'start to get page '+ str(i) + ' linkes'
                try:
                    t = threading.Thread(target=self.geturls, args=(i,))
                    threads.append(t)
                except:
                    pass
            [t.start() for t in threads]
            [t.join() for t in threads]
            if stop < links:
                if (stop + 11) > links:
                    new_stop = links - stop
                    do_thread(stop + 1, stop + new_stop)
                else:
                    do_thread(stop + 1, stop + 11)
        if links <= 1:
            links = 2
            do_thread(1, links)
        elif links < 5:
            do_thread(1, links+1)
        else:
            do_thread(1, 11)
        date = datetime.datetime.now()
        file_name = 'links\\' + str(date.date()) + '_' + str(date.hour) + str(date.minute) + str(date.second)
        if not os.path.isdir('links'):
            os.makedirs('links')
        f = open(file_name + '.txt', 'w')
        all_links = ''
        for i in self.links:
            all_links += (i + '\n')
        f.write(all_links)
        f.close()
        t2=datetime.datetime.now()
        print 'time is: ',t2-t1


    def geturls(self, num):
        br = mechanize.Browser()
        br.set_handle_equiv(True)
        br.set_handle_redirect(True)
        br.set_handle_referer(True)
        br.set_handle_robots(False)
        br.set_handle_refresh(mechanize.HTTPRefreshProcessor(), max_time=1)
        br.open('http://bama.ir/car?page='+str(num))
        html = br.response().read()
        br.close()
        doc = BeautifulSoup(html, 'html.parser')
        link = doc.select('.addetailpage-div > h2 > a')
        for i in link:
            self.links.append('http://bama.ir' + i['href'])


    def get_files(self):
        from os import listdir
        from os.path import isfile, join
        try:
            onlyfiles = [f for f in listdir('links\\') if isfile(join('links\\', f))]
            return onlyfiles
        except:
            pass
        return ['no file']


    def do_extract(self, file_name):
        t1 = datetime.datetime.now()
        self.phone_list = []
        def do_thread(start, stop):
            threads = []
            for i in range(start, stop):
                print 'proccess ' + str(i) + ' get in queeu'
                try:
                    p = threading.Thread(target=self.ex_number, args=(urls[i],))
                    threads.append(p)
                except:
                    pass
            try:
                [t.start() for t in threads]
                [t.join() for t in threads]
            except:
                pass
            if stop < (len_urls - 1):
                if (stop + 5) > len_urls - 1:
                    new_stop = (len_urls - 1) - stop
                    do_thread(stop, stop + new_stop)
                else:
                    do_thread(stop, stop + 5)

        urls = None
        with open('links\\' + file_name) as f:
            urls = f.readlines()
        len_urls = len(urls)
        if len_urls <= 0:
            return 'no link'
        if len_urls < 2:
            do_thread(0, len_urls-1)
        else:
            do_thread(0, 2)

        date = datetime.datetime.now()
        file_name = 'phones\\' + str(date.date()) + '_' + str(date.hour) + str(date.minute) + str(date.second)
        if not os.path.isdir('phones'):
            os.makedirs('phones')
        f = open(file_name + '.txt', 'w')
        all_phones = ''
        for i in self.phone_list:
            all_phones += (i + '\n')
        f.write(all_phones.encode('utf-8'))
        f.close()
        t2 = datetime.datetime.now()
        print 'time for extracting is : ', t2 - t1

    def ex_number(self, url):
            browser = mechanize.Browser()
            browser.set_handle_equiv(True)
            browser.set_handle_redirect(True)
            browser.set_handle_referer(True)
            browser.set_handle_robots(False)
            browser.set_handle_refresh(mechanize.HTTPRefreshProcessor(), max_time=1)
            browser.open(url)
            html = str(browser.response().read())
            browser.close()
            phone = ''
            code = ''
            try:
                s = html.index('$("#phone-field").text(')
                s += 23
                for i in range(s, s+200):
                    if html[i] == ')':
                        break
                    phone = phone + html[i]
            except:
                pass
            try:
                s = html.index('xxx')
                p = ''
                i = s - 15
                while i < s + 15:
                    if html[i] == '(':
                        j = i
                        while html[j] != ')':
                            code += html[j]
                            j += 1
                        code += html[j]
                        break
                    i += 1
            except:
                pass
            if code != '':
                self.phone_list.append(code.decode('utf-8') + phone.decode('utf-8')[1:-1])
            else:
                if phone != '':
                    self.phone_list.append(phone.decode('utf-8')[1:-1])
